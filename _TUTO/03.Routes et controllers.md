# Routes et controllers

> __Prérequis :__ [Utiliser une base de données](_TUTO/02.Utiliser une base de donnees.md)    
> __Objectif  :__ Comprendre comment créer des routes simples et définir les URL de vos services.

## Résumé

Nous avons défini les modèles Mongoose pour interagir avec la base de données et utilisé un script pour créer les données de base.   
Nous pouvons maintenant coder la présentation des données aux utilsateurs.

La première chose à faire est de déterminer quelles infos seront affichées dans nos pages, et ensuite, définir les URL pour retourner ces ressources.

Le schéma ci-dessous présente le flux principal des données et les choses que l'on doit implémenter quand on gère des requêtes/réponses HTTP.      
En plus des vues et routes, le schéma affiche des **controllers** - fonction qui sépare le code qui route les requêtes du code qui gère vraiment les requêtes.

Comme nous avons déjà créés les modèles, les principales choses que l'on doit maintenant créer sont :
- Les **routes** pour envoyer les requêtes supportées par notre appli vers le bon controller
- Les fonctions des controllers ppur récupérer les données depuis les modèles, créer les pages HTML et les renvoyer à l'utilisateur
- Les vues (templates) utilisées par le controller pour faire le rendu des données 

![Schéma](/_TUTO/tuto3.png)

## Routes ? C'est quoi ?

Une route est une section de code Express qui associe un verbe HTTP (`GET`, `POST`, `PUT`, ...), une adresse/modèle URL et une fonction appelée pour gérer la requête.

Nous utilisons ici le middleware `express.Router` pour créer nos routes.
Il nous permet de grouper les routes pour une partie spécifique du site et d'y accéder grâce à un préfixe commun.

## Routes pour notre site internet

Les URLs que nous utiliserons à la fin du tuto sont listées ci-dessous, où **objet** est remplacé par le nom de chacun de nos modèles de données (book, bookinstance, genre, author), **objets** est le pluriel d'**objet** et **id** est un identifiant unique qui est donné dans chaque modèle Mongoose par défaut.
- catalog/ => La page d'accueil
- catalog/<objets>/ => La liste des tous les livres, exemplaires, genres et auteurs. (ex: /catalog/books/)
- catalog/<objet>/<id> => La page de détail pour un livre, un exemplaire, un genre... (ex: /catalog/book/584493c1f4887f06c0e67d37)
- catalog/<objet>/<id>/update => Le formulaire pour mettre à jour un livre, un exemplaire, ...
- catalog/<objet>/<id>/delete => Le formulaire pour supprimer un livre, un exemplaire, ...

Vous pouvez créer les routes comme vous voulez, la disposition ici est purement arbitraire.

## Création des callbacks (fonctions de retour) pour chaque route dans les controllers

Avant de définir les routes, nous devons créer les fonctions (pour l'instant vide) qu'elles invoquerons.   
Ces callbacks seront stockés dans des modules **controller** séparés pour Books, BookInstances, Genres et Authors.
Encore une fois vous pouvez utiliser l'arborescence fichier/module que vous voulez.

On crée un dossier pour stocker nos controllers (`/controllers`) et on crée les fichiers des modules.

```bash
/popschool-tp
  /controllers
    authorController.js
    bookController.js
    bookinstanceController.js
    genreController.js
```

### Controller Author

Dans le fichier `/controllers/authorController.js` :

```js
var Author = require('../models/author');

// Display list of all Authors.
exports.author_list = function(req, res) {
    res.send('NOT IMPLEMENTED: Author list');
};

// Display detail page for a specific Author.
exports.author_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Author detail: ' + req.params.id);
};

// Display Author create form on GET.
exports.author_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author create GET');
};

// Handle Author create on POST.
exports.author_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author create POST');
};

// Display Author delete form on GET.
exports.author_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author delete GET');
};

// Handle Author delete on POST.
exports.author_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author delete POST');
};

// Display Author update form on GET.
exports.author_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update GET');
};

// Handle Author update on POST.
exports.author_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update POST');
};
```

Dès le début, ce module requière le modèle associé au controller que nous utiliserons plus tard pour accéder et mettre à jour les données.
Il exporte les fonctions pour chaque URLs que nous voulons gérer.

### Controller BookInstance

Dans le fichier `/controllers/bookinstanceController.js` :

```js
var BookInstance = require('../models/bookinstance');

// Display list of all BookInstances.
exports.bookinstance_list = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance list');
};

// Display detail page for a specific BookInstance.
exports.bookinstance_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance detail: ' + req.params.id);
};

// Display BookInstance create form on GET.
exports.bookinstance_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance create GET');
};

// Handle BookInstance create on POST.
exports.bookinstance_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance create POST');
};

// Display BookInstance delete form on GET.
exports.bookinstance_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance delete GET');
};

// Handle BookInstance delete on POST.
exports.bookinstance_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance delete POST');
};

// Display BookInstance update form on GET.
exports.bookinstance_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance update GET');
};

// Handle bookinstance update on POST.
exports.bookinstance_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: BookInstance update POST');
};
``` 

### Controller Genre

Dans le fichier `/controllers/bookinstanceController.js` :

```js
var Genre = require('../models/genre');

// Display list of all Genre.
exports.genre_list = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre list');
};

// Display detail page for a specific Genre.
exports.genre_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre detail: ' + req.params.id);
};

// Display Genre create form on GET.
exports.genre_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create GET');
};

// Handle Genre create on POST.
exports.genre_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create POST');
};

// Display Genre delete form on GET.
exports.genre_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete GET');
};

// Handle Genre delete on POST.
exports.genre_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete POST');
};

// Display Genre update form on GET.
exports.genre_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update GET');
};

// Handle Genre update on POST.
exports.genre_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update POST');
};
```

### Controller Book

Dans le fichier `/controllers/bookController.js` :

```js
var Book = require('../models/book');

exports.index = function(req, res) {
    res.send('NOT IMPLEMENTED: Site Home Page');
};

// Display list of all books.
exports.book_list = function(req, res) {
    res.send('NOT IMPLEMENTED: Book list');
};

// Display detail page for a specific book.
exports.book_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Book detail: ' + req.params.id);
};

// Display book create form on GET.
exports.book_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create GET');
};

// Handle book create on POST.
exports.book_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create POST');
};

// Display book delete form on GET.
exports.book_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete GET');
};

// Handle book delete on POST.
exports.book_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete POST');
};

// Display book update form on GET.
exports.book_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update GET');
};

// Handle book update on POST.
exports.book_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update POST');
};
```

> Commit GIT : 8ce8884a5792110a8f4ea335b418fd0edd24d8f4

## Création du module pour la route catalog

Ensuite, nous devons créer les routes pour les URLs nécessaires pour notre appli qui appeleront les fonctions des controllers que nous venons de créer.

Le squelette a déjà créé le dossier `/routes` contenant les routes pour **index** et **users**.   
Nous allons créer un autre fichier `catalog.js` dans ce dossier :

```js
var express = require('express');
var router = express.Router();

// Require controller modules.
var book_controller = require('../controllers/bookController');
var author_controller = require('../controllers/authorController');
var genre_controller = require('../controllers/genreController');
var book_instance_controller = require('../controllers/bookinstanceController');

/// BOOK ROUTES ///

// GET catalog home page.
router.get('/', book_controller.index);

// GET request for creating a Book. NOTE This must come before routes that display Book (uses id).
router.get('/book/create', book_controller.book_create_get);

// POST request for creating Book.
router.post('/book/create', book_controller.book_create_post);

// GET request to delete Book.
router.get('/book/:id/delete', book_controller.book_delete_get);

// POST request to delete Book.
router.post('/book/:id/delete', book_controller.book_delete_post);

// GET request to update Book.
router.get('/book/:id/update', book_controller.book_update_get);

// POST request to update Book.
router.post('/book/:id/update', book_controller.book_update_post);

// GET request for one Book.
router.get('/book/:id', book_controller.book_detail);

// GET request for list of all Book items.
router.get('/books', book_controller.book_list);

/// AUTHOR ROUTES ///

// GET request for creating Author. NOTE This must come before route for id (i.e. display author).
router.get('/author/create', author_controller.author_create_get);

// POST request for creating Author.
router.post('/author/create', author_controller.author_create_post);

// GET request to delete Author.
router.get('/author/:id/delete', author_controller.author_delete_get);

// POST request to delete Author.
router.post('/author/:id/delete', author_controller.author_delete_post);

// GET request to update Author.
router.get('/author/:id/update', author_controller.author_update_get);

// POST request to update Author.
router.post('/author/:id/update', author_controller.author_update_post);

// GET request for one Author.
router.get('/author/:id', author_controller.author_detail);

// GET request for list of all Authors.
router.get('/authors', author_controller.author_list);

/// GENRE ROUTES ///

// GET request for creating a Genre. NOTE This must come before route that displays Genre (uses id).
router.get('/genre/create', genre_controller.genre_create_get);

//POST request for creating Genre.
router.post('/genre/create', genre_controller.genre_create_post);

// GET request to delete Genre.
router.get('/genre/:id/delete', genre_controller.genre_delete_get);

// POST request to delete Genre.
router.post('/genre/:id/delete', genre_controller.genre_delete_post);

// GET request to update Genre.
router.get('/genre/:id/update', genre_controller.genre_update_get);

// POST request to update Genre.
router.post('/genre/:id/update', genre_controller.genre_update_post);

// GET request for one Genre.
router.get('/genre/:id', genre_controller.genre_detail);

// GET request for list of all Genre.
router.get('/genres', genre_controller.genre_list);

/// BOOKINSTANCE ROUTES ///

// GET request for creating a BookInstance. NOTE This must come before route that displays BookInstance (uses id).
router.get('/bookinstance/create', book_instance_controller.bookinstance_create_get);

// POST request for creating BookInstance. 
router.post('/bookinstance/create', book_instance_controller.bookinstance_create_post);

// GET request to delete BookInstance.
router.get('/bookinstance/:id/delete', book_instance_controller.bookinstance_delete_get);

// POST request to delete BookInstance.
router.post('/bookinstance/:id/delete', book_instance_controller.bookinstance_delete_post);

// GET request to update BookInstance.
router.get('/bookinstance/:id/update', book_instance_controller.bookinstance_update_get);

// POST request to update BookInstance.
router.post('/bookinstance/:id/update', book_instance_controller.bookinstance_update_post);

// GET request for one BookInstance.
router.get('/bookinstance/:id', book_instance_controller.bookinstance_detail);

// GET request for list of all BookInstance.
router.get('/bookinstances', book_instance_controller.bookinstance_list);

module.exports = router;
```

Les routes sont définies en utilisant soit la méthode `.get()` soit la méthode `.post()` sur l'objet router. Tous les chemins sont définis en utilisant des strings.
Les fonctions gérant les routes sont toutes importées depuis les controllers. 

> Commit GIT : 02fa472857d5d7affd49a6894a8a16f852f91dc7

### Mise à jour du module de route index

Maintenant que l'on a définit nos routes, il reste encore la route vers la page du début.   
A la place, nous allons la rediriger vers notre nouvelle page index que nous avons créé avec le chemin `/catalog`.

Ouvrez le fichier `/routes/index.js` et remplacer la route existante par celle-ci :
```js
// GET home page.
router.get('/', function(req, res) {
  res.redirect('/catalog');
});
``` 

> Commit GIT : df8fb0e5787078079470f1786fec079f02aec6b3

### Mise à jour d'app.js

La dernière étape est d'ajouter nos routes à la chaîne des middlewares Express. Cela se fait dans `app.js`.

On ajoute la troisième ligne :
```js
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var catalogRouter = require('./routes/catalog');
```

Ensuite, on ajoute le router catalog dans la chaine des middlewares en dessous des autres (on ajoute la 3ème ligne) :
```js
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/catalog', catalogRouter);
```

> Commit GIT : 5c768e43baf531553f2ef66a4de4684354ae0f0a

### On teste les routes

On (re)lance le serveur. ==> [Voir ici pour rappel de la commande](/_TUTO/01.Creation d'un squelette de site internet.md#permettre-le-red%C3%A9marrage-du-serveur-en-cas-de-changements-dans-les-fichiers)

Ensuite, naviguez vers différentes adresses pour voir si vous n'avez pas d'erreur 404.
Une petite liste rapide :
- http://localhost:3000/
- http://localhost:3000/catalog
- http://localhost:3000/catalog/books
- http://localhost:3000/catalog/bookinstances/
- http://localhost:3000/catalog/authors/
- http://localhost:3000/catalog/genres/
- http://localhost:3000/catalog/book/5846437593935e2f8c2aa226
- http://localhost:3000/catalog/book/create

___

[Partie 4 : Afficher les données](/_TUTO/04.Afficher les donnees.md)
